# Programme de la semaine 12

1750m

## Échauffement 450m
- 2x( 50m CR + 50m BR + 50m Dos )
- Jambes 3x( 25m ondulations + 25 NL) (ondulations coté, ventre, dos)

## Séance 1150m
- 50m CR + 50m BR + 50m Dos
- 12 x 25m CR (lent, moyen, rapide)
- 100m BR
- 200m Educ Dos PB
- 100m Dos Jambes avec planche
- 100m Dos
- 100m CR Jambes avec planche 
- 100m CR

## Récupération 100

- 100m NL
