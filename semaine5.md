# Programme de la semaine 5

1500m

## Échauffement 450m

- 50m BR
- 50m Dos
- 50m CR

- 50m BR + 50m NL
- 75m Dos + 25m NL
- 100m CR

## Séance 700m

- 50m educ BR jambes + 50m BR
- 50m educ CR + 50m CR
- 50m educ Dos + 50m Dos

- 2 x ( 25m jambes BR + 25 BR)
- 2 x ( 25m jambes CR + 25 CR)
- 2 x ( 25m jambes Dos + 25 Dos)

- 100m NL

## Récupération 350

- 50m BR + 50m CR + 50m Dos + 50 NL
- 25m BR + 25m CR + 25m Dos + 25 NL
- 50m NL
