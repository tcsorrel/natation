# Programme de la semaine 10

1750m

## Échauffement 350m

- 100m 4 Nages (PAP, Dos, BR, CR)
- 50m BR + 50m Dos + 50m CR 
- 100m 4 Nages (PAP, Dos, BR, CR)

## Séance 1100m

- 100m 4 Nages jambes avec planche
- 100m 4 Nages educ avec planche
- 100m 4 Nages pull boy
- 100m 4 Nages NC

- 25m PAP
- 50m Dos
- 25m BR
- 50m CR
- 50m NL

- Avec plames
 - 2x( 100m CR ratttrapé + 100m Dos rattrapé )
 - 2x( 25m odulation PAP + 25m Dos )

## Récupération 300

- 2x( 50m BR + 50m Dos + 50m CR )
