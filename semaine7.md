# Programme de la semaine 7

1600m

## Échauffement 400m

- 75m CR + 25m Dos
- 75m BR + 25m Pap (ondulation ou bras CR)
- 75m Dos + 25m CR
- 75m Pap (ondulation ou bras CR) + 25m BR

## Séance 1000m

- Avec plaquettes
 - 100m educ CR
 - 100m CR rattrapé
 - 100m educ Dos
 - 100m Dos rattrapé

- Avec Pull Boy
 - 100m CR
 - 100m Dos
 - 100m CR
 - 100m Dos

- 100m educ BR
- 100m BR

## Récupération 200

- 200m CR
