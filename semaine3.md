# Programme de la semaine 3

1750m

## Échauffement 300m
2 x (
- 50m NL
- 50m jambes NL
- 50m NL PullBoy
)

## Séance 1250m
2 x (
- 25 educ CR + 50 CR + 25 jambes CR
- 25 educ BR + 50 BR + 25 jambes BR
)

6 x 50m avec 20" de repos entre:
- 50 CR rapide + 50 NL moyen
- 50 BR rapide + 50 NL moyen
- 50 dos rapide + 50 NL moyen

2 x (
- 100m CR
- 100m NL
)

- 6 x 25m CR à vitesse croissante avec 20" de repos entre

## Récupération 200

- 200 CR
