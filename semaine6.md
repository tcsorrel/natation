# Programme de la semaine 6

1900m

## Échauffement 400m

- 50m CR
- 50m Dos
- 50m BR
- 50m CR

- 2 x (
  - 25m jambes CR
  - 25m jambes Dos
  - 25m jambes BR
  - 25m jambes CR
)

## Séance 1400m

- 50m CR + 50m BR

- 3 x (
  - 75m Dos Educ pull boy 1 main au genou
  - 25m Dos pullboy
)

- 100m Dos

- 25 jambes Dos avec mains aux genoux
- 25 Dos
- 25 jambes CR et bras BR
- 25 CR

- avec palmes (
  - 100m jambes Dos avec planche aux genoux
  - 100m Dos
  - 100m jambes CR
  - 100m CR

  - 2 x (
    - 100m Dos
    - 50m CR
  )
)

- 100m Dos

## Récupération 100

- 100m NL
