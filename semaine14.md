# Programme de la semaine 12

1500m

## Échauffement 500m
- 100m CR (50 NC, 50 jambes)
- 100m BR (50 NC, 50 jambes)
- 100m Dos (50 NC, 50 jambes)
- 100m PAP (25 Educ, 25 jambes, 50 NC)
- 100m NL

## Séance 700m
- 100m CR pullboy plaquette
- 50m CR jambes
- 100m CR (50 Educ, 50 NC)
- 50m CR jambes
- 100m CR (50 rattrapé, 50 NC)
- 50m CR jambes
- 100m CR (25 rapide, 25 relaché)
- 50m CR jambes
- 100m NL

## Récupération 300m

- 2x100m 4 nages
- 100m NL
